#!/bin/sh

cmake \
    -DCMAKE_TOOLCHAIN_FILE=/home/egor/Downloads/android.toolchain.cmake \
    -DANDROID_NDK=/home/egor/Downloads/android-ndk-r10d/ \
    -DANDROID_NATIVE_API_LEVEL=android-21 \
    -DNATIVE_BUILD_DIR=/home/egor/dev/firebird_cmake/build \
    -H. \
    -Bbuild_android

