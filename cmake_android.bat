@echo off
@set PATH=%PATH%;g:\dev\cygwin64\bin\
cmake ^
    -DCMAKE_TOOLCHAIN_FILE=g:\dev\android-cmake\android.toolchain.cmake ^
    -DANDROID_NDK=g:\dev\android-ndk-r10d\ ^
    -DANDROID_NATIVE_API_LEVEL=android-21 ^
    -DCMAKE_MAKE_PROGRAM=g:\dev\android-ndk-r10d\prebuilt\windows-x86_64\bin\make.exe ^
    -DNATIVE_BUILD_DIR=win ^
    -H. ^
    -Bwin_android -G "Unix Makefiles"
cmake --build win_android